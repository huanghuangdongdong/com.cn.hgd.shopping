package qf.shopping.web.filter;
/**
 * @author: COS
 * @time: 2023/5/4 9:46
 * @description:
 */

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@WebFilter(filterName = "Filter01_Charset",value = "/*")
public class Filter01_Charset implements Filter {
    public void init(FilterConfig config) throws ServletException {
    }

    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        String requestURI = req.getRequestURI();
        if (check(requestURI)) {
            // 设置字符编码
            req.setCharacterEncoding("utf-8");
            resp.setContentType("text/html;charset=utf-8");
        }
        chain.doFilter(req, resp);
    }


    public boolean check(String path){
        //判断路径 如果不是 .html .css .js / .png 这些文件就需要进行字符编码的过滤
        List<String> list = Arrays.asList(".html", ".css", ".js", "/", ".png");
        //遍历查询
        for (String s : list) {
            if (path.endsWith(s)) {
                // 判断 请求的路径上有 静态资源, 返回false出去不做编码设置
                return false;
            }
        }
        return true;
    }
}
