package qf.shopping.web.controller;

import com.alibaba.fastjson.JSON;
import org.apache.commons.beanutils.BeanUtils;
import qf.shopping.common.Const;
import qf.shopping.common.ResponseCode;
import qf.shopping.common.ResponseResult;
import qf.shopping.entity.User;
import qf.shopping.services.UserServices;
import qf.shopping.services.impl.UserServicesImpl;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.Map;

/**
 * @author 黄贵冬
 * 这个usercontroller 来继承basecontroller就不用直接继承HttpServlet
 * 不然每个要写父类中的步骤，代码冗余 复用性不高
 */
@WebServlet("/user")
public class UserController extends BaseController{
    private UserServices us=new UserServicesImpl();
   //登录
    public String login(HttpServletRequest req, HttpServletResponse resp){
        //接收用户提交的数据
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        //访问业务层
         ResponseResult rr=  us.dologin(username,password);
        if (rr.getCode()== ResponseCode.SUCCESS.getCode()) {
            // 成功 保存信息到session
            req.getSession().setAttribute(Const.LOGIN_INFO,rr.getT());
            //重定向
            return Const.REDIRECT+"/index.jsp";
        }
        //失败就转发登录页面
             req.setAttribute("msg",rr);
        return  Const.FORWARD+"login.jsp";


    }
    public String reg(HttpServletRequest req, HttpServletResponse resp) throws InvocationTargetException, IllegalAccessException {
        //获取用户提交的数据
        Map<String, String[]> parameterMap = req.getParameterMap();
        //创建数据要封装的对象
        User user=new User();
        //使用工具类进行封装
        BeanUtils.populate(user,parameterMap);
        //访问业务层
      ResponseResult rr=  us.reg(user);
      //存储数据到作用域
        if (rr.getCode()== ResponseCode.SUCCESS.getCode()) {
            return Const.REDIRECT+"registerSuccess.jsp";
        }
        //进入前端展示
       req.setAttribute(Const.MSG,rr);
        return Const.MESSAGE;
    }

    public String checkUserName(HttpServletRequest req, HttpServletResponse resp) throws SQLException {
        //获取用户提交的数据
        String username = req.getParameter("username");
        System.out.println(username);
        //访问业务层   json数据
       ResponseResult result=us.checkUserNameNewMethod(username);
        //根据业务层不同结果  给出不同响应
        String jsonString = JSON.toJSONString(result);
        return  jsonString;

    }


    //用户登录 检查用户是否存在
   /** 老的方法
    * public String checkUserName(HttpServletRequest req, HttpServletResponse resp) throws SQLException {
        //获取用户提交的数据
        String username = req.getParameter("username");
        //访问业务层
        User user= us.checkUserName(username);
        //根据业务层不同结果  给出不同响应
        if (user!=null) {
            return "1";
        }
        return "0";
    }
    */

}
