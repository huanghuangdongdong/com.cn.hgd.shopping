package qf.shopping.web.controller;

import qf.shopping.common.Const;
import qf.shopping.common.ResponseResult;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author 黄贵冬
 *
 */

public class BaseController extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //获取子类传递过来的参数
        String method = req.getParameter("method");
       System.out.println(method);
        try {
            //利用反射 来调取方法
            //获取类对象  利用实例对象来获取
            Class<? extends BaseController> ac= this.getClass();
            //用类对象来获取类中的方法
            Method m = ac.getMethod(method, HttpServletRequest.class, HttpServletResponse.class);
            //利用反射调用方法 invoke(调用方法的对象，参数)
            String  invoke = (String) m.invoke(this, req, resp);
            //每个方法执行后都会有不同的操作 转发 重定向 字符串输出
            //所以可以约定每个方法都有一个返回值   根据返回值的不同来确定执行不同的响应 转发 重定向还是字符串输出
            //判断invoke是否包含约定的 forward：  redirect： 这里可以在接口设置一个常量
            if(invoke.contains(Const.FORWARD)){
                //转发到
                req.getRequestDispatcher(invoke.replace(Const.FORWARD,"")).forward(req,resp);
            } else if (invoke.contains(Const.REDIRECT)) {
                //就重定向
                resp.sendRedirect(invoke.replace(Const.REDIRECT,""));
            }else {
                //字符串写出
                resp.getWriter().println(invoke);
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
            //异常处理位置 处理调用出现找不到方法异常
            req.setAttribute("msg", ResponseResult.createFail("啊服务器出错啦 啊哈哈哈"));
            req.getRequestDispatcher("message.jsp").forward(req,resp);
        }catch (Exception e) {
            e.printStackTrace();
            //异常捕获
            req.setAttribute("msg", ResponseResult.createFail("啊喔！你要找的页面丢失了！！！"));
            req.getRequestDispatcher("message.jsp").forward(req,resp);
        }
    }
}
