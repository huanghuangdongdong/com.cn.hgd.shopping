package qf.shopping.web.controller;

import com.alibaba.fastjson.JSON;
import qf.shopping.common.Const;
import qf.shopping.common.ResponseResult;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author 黄贵冬
 */
@WebServlet("/sys")
public class SystemController extends BaseController{

    public String checkCode(HttpServletRequest req, HttpServletResponse resp){
        //获取用户提交的数据
        String code = req.getParameter("code");
        //获取session作用域中的数据
       String  msgCode = (String) req.getSession().getAttribute(Const.MESSAGE_CODE);
        //判断
        if (msgCode.equals(code)) {
            return JSON.toJSONString(ResponseResult.createSuccess("验证成功！"));
        }
        return  JSON.toJSONString(ResponseResult.createFail("验证码错误！"));
    }

}
