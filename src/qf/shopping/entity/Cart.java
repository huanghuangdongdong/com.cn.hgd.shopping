package qf.shopping.entity;

public class Cart {

  private int cid;
  private int uid;
  private int pid;
  private double ccount;
  private int cnum;

  public int getCid() {
    return cid;
  }

  public void setCid(int cid) {
    this.cid = cid;
  }

  public int getUid() {
    return uid;
  }

  public void setUid(int uid) {
    this.uid = uid;
  }

  public int getPid() {
    return pid;
  }

  public void setPid(int pid) {
    this.pid = pid;
  }

  public double getCcount() {
    return ccount;
  }

  public void setCcount(double ccount) {
    this.ccount = ccount;
  }

  public int getCnum() {
    return cnum;
  }

  public void setCnum(int cnum) {
    this.cnum = cnum;
  }

  public Cart() {
  }

  public Cart(int cid, int uid, int pid, double ccount, int cnum) {
    this.cid = cid;
    this.uid = uid;
    this.pid = pid;
    this.ccount = ccount;
    this.cnum = cnum;
  }
}
