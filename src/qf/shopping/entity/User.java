package qf.shopping.entity;

public class User {

  private int uid;
  private String uname;
  private String upassword;
  private String uemail;
  private String usex;
  private int ustatus;
  private String ucode;
  private int urole;

  public int getUid() {
    return uid;
  }

  public void setUid(int uid) {
    this.uid = uid;
  }

  public String getUname() {
    return uname;
  }

  public void setUname(String uname) {
    this.uname = uname;
  }

  public String getUpassword() {
    return upassword;
  }

  public void setUpassword(String upassword) {
    this.upassword = upassword;
  }

  public String getUemail() {
    return uemail;
  }

  public void setUemail(String uemail) {
    this.uemail = uemail;
  }

  public String getUsex() {
    return usex;
  }

  public void setUsex(String usex) {
    this.usex = usex;
  }

  public int getUstatus() {
    return ustatus;
  }

  public void setUstatus(int ustatus) {
    this.ustatus = ustatus;
  }

  public String getUcode() {
    return ucode;
  }

  public void setUcode(String ucode) {
    this.ucode = ucode;
  }

  public int getUrole() {
    return urole;
  }

  public void setUrole(int urole) {
    this.urole = urole;
  }

  public User() {
  }

  public User(int uid, String uname, String upassword, String uemail, String usex, int ustatus, String ucode, int urole) {
    this.uid = uid;
    this.uname = uname;
    this.upassword = upassword;
    this.uemail = uemail;
    this.usex = usex;
    this.ustatus = ustatus;
    this.ucode = ucode;
    this.urole = urole;
  }
}
