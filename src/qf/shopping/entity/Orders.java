package qf.shopping.entity;

import java.util.Date;

public class Orders {

  private String oid;
  private int uid;
  private int aid;
  private double ocount;
  private Date otime;
  private int ostate;

  public String getOid() {
    return oid;
  }

  public void setOid(String oid) {
    this.oid = oid;
  }

  public int getUid() {
    return uid;
  }

  public void setUid(int uid) {
    this.uid = uid;
  }

  public double getOcount() {
    return ocount;
  }

  public void setOcount(double ocount) {
    this.ocount = ocount;
  }

  public Date getOtime() {
    return otime;
  }

  public void setOtime(Date otime) {
    this.otime = otime;
  }

  public int getOstate() {
    return ostate;
  }

  public void setOstate(int ostate) {
    this.ostate = ostate;
  }

  public int getAid() {
    return aid;
  }

  public void setAid(int aid) {
    this.aid = aid;
  }

  public Orders() {
  }

  public Orders(String oid, int uid, int aid, double ocount, Date otime, int ostate) {
    this.oid = oid;
    this.uid = uid;
    this.aid = aid;
    this.ocount = ocount;
    this.otime = otime;
    this.ostate = ostate;
  }
}
