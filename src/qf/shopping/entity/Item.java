package qf.shopping.entity;

public class Item {

  private int iid;
  private String oid;
  private int pid;
  private double icount;
  private int inum;

  public int getIid() {
    return iid;
  }

  public void setIid(int iid) {
    this.iid = iid;
  }

  public String getOid() {
    return oid;
  }

  public void setOid(String oid) {
    this.oid = oid;
  }

  public int getPid() {
    return pid;
  }

  public void setPid(int pid) {
    this.pid = pid;
  }

  public double getIcount() {
    return icount;
  }

  public void setIcount(double icount) {
    this.icount = icount;
  }

  public int getInum() {
    return inum;
  }

  public void setInum(int inum) {
    this.inum = inum;
  }

  public Item() {
  }

  public Item(int iid, String oid, int pid, double icount, int inum) {
    this.iid = iid;
    this.oid = oid;
    this.pid = pid;
    this.icount = icount;
    this.inum = inum;
  }
}
