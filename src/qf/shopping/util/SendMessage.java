package qf.shopping.util;

import com.baidubce.http.ApiExplorerClient;
import com.baidubce.http.AppSigner;
import com.baidubce.http.HttpMethodName;
import com.baidubce.model.ApiExplorerRequest;
import com.baidubce.model.ApiExplorerResponse;
import qf.shopping.common.Const;


import java.util.Random;


public class SendMessage {


    public static String send(String phone) {
        String path = "https://dfsms.api.bdymkt.com/send_sms";
        ApiExplorerRequest request = new ApiExplorerRequest(HttpMethodName.POST, path);
        request.setCredentials(Const.ACCESS_KEY, Const.SECRET_KEY);

        // 设置header参数
        request.addHeaderParameter("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
        String code = "";
        for (int i = 0; i < 4; i++) {
            int num = new Random().nextInt(10);
            code+=num;
        }
        // 设置jsonBody参数
        String jsonBody = "phone_number=" + phone + "&template_id=TPL_0001&content=code:"+code+",expire_at:5";
        request.setJsonBody(jsonBody);

        ApiExplorerClient client = new ApiExplorerClient(new AppSigner());

        try {
            ApiExplorerResponse response = client.sendRequest(request);
            // 返回结果格式为Json字符串
            //System.out.println(response.getResult());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return code;
    }

    public static void main(String[] args) {

        String send = SendMessage.send("13480849706");
        System.out.println(send);

    }

}
