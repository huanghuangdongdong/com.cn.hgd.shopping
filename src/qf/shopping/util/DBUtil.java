package qf.shopping.util;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidDataSourceFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

public class DBUtil {

    //获取数据源
    private static final ThreadLocal<Connection> THREAD_LOCAL = new ThreadLocal();
    //声明德鲁伊
    private static DruidDataSource ds;

    static {
        //读取配置文件
        try {
            Properties pro = new Properties();
            pro.load(DBUtil.class.getResourceAsStream("/jdbc.properties"));
            //创建数据源
            ds = (DruidDataSource) DruidDataSourceFactory.createDataSource(pro);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
            //获取数据源方法
        public static DruidDataSource getDateSource() {
            return ds;

        }

     //获取连接方法
    public static Connection getConnection(){
             //判断connection是否为空
        Connection connection = THREAD_LOCAL.get();
        try {
            if(connection==null){
                  //获取连接
                connection=ds.getConnection();
                //把连接存入连接池
                THREAD_LOCAL.set(connection);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  connection;

    }


    //关闭资源
    public  static  void  closeAll(ResultSet rs, PreparedStatement ps, Connection con){
         //判断是否为空  空就不需关闭
        try {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
                //这里要移除连接
                THREAD_LOCAL.remove();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }


    //事务开启 提交  回滚
    public static void begin(){
        try {
            Connection connection = getConnection();
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    public static void commit(){
        try {
            Connection connection = getConnection();
            connection.commit();
            closeAll(null,null,connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void rollback(){
        try {
            Connection connection = getConnection();
            connection.rollback();
            closeAll(null,null,connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


}
