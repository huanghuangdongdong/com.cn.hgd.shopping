package qf.shopping.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StringUtil {

    private static SimpleDateFormat sdf = new SimpleDateFormat();

    public static String dateToString(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }


    public static Date stringToDate(String date) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            return sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Date stringToDate(String date, String paading) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(paading);
            return sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }


    //判断字符串是否为空
    public static boolean isNotEmpty(String... strs) {
        for (String str : strs) {
            if (str == null || str.trim().length() == 0) {
                return false;
            }
        }
        return true;
    }

    public static Integer stringToInt(String val) {
        int i = 0;
        try {
            i = isNotEmpty(val) ? Integer.valueOf(val) : -1;
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return -1;
        }
        return i;

    } public static Double stringToDouble(String val) {
        Double i = 0.0;
        try {
            i = isNotEmpty(val) ? Double.valueOf(val) : -1;
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return -1.0;
        }
        return i;

    }


}
