package qf.shopping.common;

/**
 * @author 黄贵冬
 */
public class ResponseResult<T> {

    private int code;
    private String msg;

private T t;
    //设置默认的成功响应代码
   public static ResponseResult  createSuccess() {
       return  new ResponseResult(ResponseCode.SUCCESS.getCode(),ResponseCode.SUCCESS.getMsg());
    }
    //设置默认成功代码和自定义信息
    public static ResponseResult  createSuccess(String msg) {
        return  new ResponseResult(ResponseCode.SUCCESS.getCode(),msg);
    }
    //设置默认自定义成功代码和自定义信息
    public static ResponseResult  createSuccess(int code, String msg) {
        return  new ResponseResult(code,msg);
    }
    //这只默认的成功响应代码，响应信息 和响应数据
    public static<T> ResponseResult  createSuccess(T data) {
        return  new ResponseResult(ResponseCode.SUCCESS.getCode(),ResponseCode.SUCCESS.getMsg(),data);
    }

    //同理设置失败的响应代码
    //设置默认的失败响应代码
    public static ResponseResult  createFail() {
        return  new ResponseResult(ResponseCode.FAIL.getCode(),ResponseCode.FAIL.getMsg());
    }
    //设置默认失败代码和自定义信息
    public static ResponseResult  createFail(String msg) {
        return  new ResponseResult(ResponseCode.FAIL.getCode(),msg);
    }
    //设置默认自定义失败代码和自定义信息
    public static ResponseResult  createFail(int code, String msg) {
        return  new ResponseResult(code,msg);
    }
    //这只默认的是失败响应代码，响应信息 和响应数据
    public static<T> ResponseResult  createFail(T data) {
        return  new ResponseResult(ResponseCode.FAIL.getCode(),ResponseCode.FAIL.getMsg(),data);
    }




    public ResponseResult() {
    }

    public ResponseResult(T t) {
        this.t = t;
    }

    public ResponseResult(int code, T t) {
        this.code = code;
        this.t = t;
    }

    public ResponseResult(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public ResponseResult(int code, String msg, T t) {
        this.code = code;
        this.msg = msg;
        this.t = t;
    }

    public static<T> ResponseResult createResult(int code, String msg, T data) {
       return new ResponseResult(code,msg,data);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }
}
