package qf.shopping.common;

/**
 * @author 黄贵冬
 */

public enum ResponseCode {
    //设置响应代码
    FAIL(0,"失败"),
    SUCCESS(1,"成功"),
    IS_LOGIN(2,"已登录"),
    NOT_LOGIN(3,"未登录"),
    NOT_FIND(4,"404,你要访问的资源不存在！");
    private int code;
    private String msg;

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    ResponseCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
