package qf.shopping.common;

public interface Const {
    String FORWARD = "forward:";
    String REDIRECT = "redirect:";
    String MESSAGE_CODE = "message_code";
    String MSG = "msg";

    // 进入前端显示消息的页面
    String MESSAGE = "forward:message.jsp";
    String ACCESS_KEY = "";
    String SECRET_KEY = "";
    String LOGIN_INFO = "login_info";

    public interface UserRole {
        //账户已激活
        int ACTIVE = 1;
        //账户未激活
        int NOT_ACTIVE = 0;
        //普通用户
        int USER = 0;
        //管理员
        int ADMIN = 1;


    }

}
