package qf.shopping.services.impl;


import qf.shopping.common.Const;
import qf.shopping.common.ResponseCode;
import qf.shopping.common.ResponseResult;
import qf.shopping.dao.UserDao;
import qf.shopping.dao.impl.UserDaoImpl;
import qf.shopping.entity.User;
import qf.shopping.util.*;

import java.sql.SQLException;
import java.util.Random;

/**
 * @author 黄贵冬
 */
public class UserServicesImpl implements qf.shopping.services.UserServices {
    //创建dao对象
    private UserDao ud=new UserDaoImpl();
//    @Override
//    public User checkUserName(String username) throws SQLException {
//        //判断用户是否为空
//        try {
//            if (username != null) {
//                //发送dao
//                User user=ud.checkUserName(username);
//                return user;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return null;
//    }

    @Override
    public ResponseResult checkUserNameNewMethod(String username) {
        try {
            //判断非空
            if (StringUtil.isNotEmpty(username)) {
                //发送dao
               User user= ud.checkUserNameNewMethod(username);
                System.out.println(user);
                if (user != null) {
                    //给出一个响应
                  return   ResponseResult.createSuccess("用户名不可用！");
                }
            }else {
                //考虑用户名为空的
                return  ResponseResult.createResult(ResponseCode.SUCCESS.getCode(),"用户名不能为空",null);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //未注册的可用名
        return ResponseResult.createFail("用户名可用");
    }

    @Override
    public ResponseResult reg(User user) {
        try {
            //判断非空
            if (user != null) {
                //设置用户激活状态 普通用户 邮件激活
                user.setUrole(Const.UserRole.USER);
                user.setUstatus(Const.UserRole.NOT_ACTIVE);
                user.setUcode(RandomUtils.createActive());
                //密码加密
                user.setUpassword(MD5Utils.md5(user.getUpassword()));
                //发送dao新增
                if (ud.insrtUser(user)>0) {
                   //注册成功后再发送邮件  设置线程发送提升用户体验
                    new Thread(()-> EmailUtils.sendEmail(user)).start();
                    return ResponseResult.createSuccess("注册成功！");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseResult.createFail("注册失败！");
    }

    @Override
    public ResponseResult dologin(String username, String password) {
           //判断非空
        try {
            if (StringUtil.isNotEmpty(username,password)) {
                //发送dao
                User user=ud.getUserByUserName(username);
                //非空   用户密码是加密过的 要比较的话就先将用户输入的密码加密再和数据库里的比较
                String s = MD5Utils.md5(password);
                //判断
                if (user != null&&user.getUpassword().equals(s)) {
                    //判断是否激活
                    if (user.getUstatus()== Const.UserRole.ACTIVE) {
                        //登录成功
                        return ResponseResult.createSuccess(user);
                    }else {
                        //失败
                        return ResponseResult.createFail("账户尚未激活！");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //输入错误
        return ResponseResult.createFail("账户或密码错误！请核对后再试！");
    }
}

