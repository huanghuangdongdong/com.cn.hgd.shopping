package qf.shopping.services;

import qf.shopping.common.ResponseResult;
import qf.shopping.entity.User;

import java.sql.SQLException;

public interface UserServices {
   // User checkUserName(String username) throws SQLException;

    ResponseResult checkUserNameNewMethod(String username);

    ResponseResult reg(User user);

    ResponseResult dologin(String username, String password);
}
