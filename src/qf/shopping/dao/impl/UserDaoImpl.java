package qf.shopping.dao.impl;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import qf.shopping.dao.UserDao;
import qf.shopping.entity.User;
import qf.shopping.util.DBUtil;

import java.sql.SQLException;

/**
 * @author 黄贵冬
 */
public class UserDaoImpl implements UserDao {
    //创建一个数据源对象
    private QueryRunner qr=new QueryRunner(DBUtil.getDateSource());
   // @Override
//    public User checkUserName(String username) throws SQLException {
//        String sql = "select " +
//                        "`u_id` 'uid',`u_name` 'uname',`u_password` 'upassword'," +
//                        "`u_email` 'uemail',`u_sex` 'usex',`u_status` 'ustatus'," +
//                        "`u_code` 'ucode',`u_role` 'urole'" +
//                        "from user  " +
//                        "where u_name=?";
//        return qr.query(sql,new BeanHandler<>(User.class),username);
//    }

    //新方法验证用户名存在与否
    @Override
    public User checkUserNameNewMethod(String username) throws SQLException {
        String sql = "select " +
                "`u_id` 'uid',`u_name` 'uname',`u_password` 'upassword'," +
                "`u_email` 'uemail',`u_sex` 'usex',`u_status` 'ustatus'," +
                "`u_code` 'ucode',`u_role` 'urole'" +
                "from user  " +
                "where u_name=?";
        return qr.query(sql,new BeanHandler<>(User.class),username);
    }
    //新用户注册
    @Override
    public int insrtUser(User user) throws SQLException {
        String sql="insert into user values (default,?,?,?,?,?,?,?)";
       return qr.update(sql,
               user.getUname(),
               user.getUpassword(),
               user.getUemail(),
               user.getUsex(),
               user.getUstatus(),
               user.getUcode(),
               user.getUrole());
    }

    @Override
    public User getUserByUserName(String username) throws SQLException {
        String sql = "select " +
                "`u_id` 'uid',`u_name` 'uname',`u_password` 'upassword'," +
                "`u_email` 'uemail',`u_sex` 'usex',`u_status` 'ustatus'," +
                "`u_code` 'ucode',`u_role` 'urole'" +
                "from user  " +
                "where u_name=?";
        return qr.query(sql,new BeanHandler<>(User.class),username);
    }
}
