package qf.shopping.dao;

import qf.shopping.entity.User;

import java.sql.SQLException;

public interface UserDao {
   // User checkUserName(String username) throws SQLException;

    User checkUserNameNewMethod(String username) throws SQLException;

    int  insrtUser(User user) throws SQLException;

    User getUserByUserName(String username) throws SQLException;
}
